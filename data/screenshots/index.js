import puppeteer from 'puppeteer'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { readdir } from 'fs/promises'

const __dirname = dirname(fileURLToPath(import.meta.url))

function getStyle(file) {
  let style = [`.content, .tab {
  background-image: url(./img/${file});
}`]
  if (file === 'man-page-git-for-each-ref.png') {
    style.push(`.content {
  border-color: #383838;
}`)
    style.push(`.titlebar-mask, .tab {
  display: none;
}`)
  }
  return style.join('\n')
}

try {
  const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] })
  const page = await browser.newPage()
  await page.goto(`file://${join(__dirname, 'index.html')}`)
  const files = await readdir(join(__dirname, 'img'))
  for (const file of files) {
    if (file === 'filetype-icon.png' || file === 'intellij-asciidoc-editor.png') continue
    await page.addStyleTag({ content: getStyle(file) })
    await page.screenshot({ path: join(__dirname, 'dist', file), fullPage: true, omitBackground: true })
    await page.reload() // reset styles
  }
  await browser.close()
} catch (err) {
  console.error('Something went wrong!', err)
  process.exit(1)
}
