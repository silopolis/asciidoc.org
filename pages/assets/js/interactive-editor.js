import Asciidoctor from '@asciidoctor/core'
import { EditorState } from '@codemirror/state'
import { EditorView, keymap } from '@codemirror/view'
import { lineNumbers } from '@codemirror/gutter'
import { standardKeymap } from '@codemirror/commands'
import { history, historyKeymap } from '@codemirror/history'
import hljs from 'highlight.js'

export function init () {
  const processor = Asciidoctor()
  const editor = document.getElementById('editorCode')
  const input = editor.firstElementChild.innerText
  editor.innerHTML = ''
  const editorPreview = document.getElementById('editorPreview')
  const state = EditorState.create({
    doc: input,
    extensions: [
      lineNumbers(),
      keymap.of(
        ...standardKeymap,
        ...historyKeymap
      ),
      history(),
      EditorView.updateListener.of((update) => {
        if (update.docChanged) {
          debounce(() => convertToHtml(), 500)()
        }
      })
    ]
  })

  const view = new EditorView({ state })
  editor.appendChild(view.dom)

  function convertToHtml () {
    editorPreview.innerHTML = processor.convert(view.state.doc.text, {
      attributes: { showtitle: '', icons: 'font' }
    })
    ;[].slice.call(editorPreview.querySelectorAll('pre.highlight > code')).forEach(function (el) {
      hljs.highlightBlock(el)
    })
  }

  function debounce (func, duration) {
    let timeout
    return function (...args) {
      const effect = () => {
        timeout = null
        return func.apply(this, args)
      }
      clearTimeout(timeout)
      timeout = setTimeout(effect, duration)
    }
  }

  convertToHtml()
}
