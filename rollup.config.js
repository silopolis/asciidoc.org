import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import { babel } from '@rollup/plugin-babel'
import copy from 'rollup-plugin-copy'

function wrap () {
  return {
    name: 'rollup-plugin-wrap',
    transform (content, id) {
      if (id.match(/highlightjs-line-numbers.js[\\/]src[\\/]highlightjs-line-numbers.js$/)) {
        content = `export default function init(hljs) {

  window.hljs = hljs
;${content}
}`
        return {
          code: content,
          map: { mappings: '' }
        }
      }
      return null
    }
  }
}

export default {
  input: 'pages/assets/js/main.js',
  output: {
    file: '_site/assets/js/main.bundle.js',
    format: 'iife',
    strict: false // Opal does not work in strict mode :|
  },
  plugins: [
    copy({
      targets: [
        {
          src: [
            'node_modules/@fontsource/inter/files/inter-latin-400-normal.woff2',
            'node_modules/@fontsource/inter/files/inter-latin-500-normal.woff2',
            'node_modules/@fontsource/inter/files/inter-latin-700-normal.woff2',
            'node_modules/@fontsource/inter/files/inter-latin-900-normal.woff2',
            'node_modules/@fontsource/urbanist/files/urbanist-latin-400-normal.woff2',
            'node_modules/@fontsource/urbanist/files/urbanist-latin-500-normal.woff2',
            'node_modules/@fontsource/urbanist/files/urbanist-latin-700-normal.woff2',
            'node_modules/@fontsource/urbanist/files/urbanist-latin-900-normal.woff2',
            'node_modules/@fontsource/source-code-pro/files/source-code-pro-latin-400-normal.woff2',
            'node_modules/@fontsource/source-code-pro/files/source-code-pro-latin-500-normal.woff2',
            'node_modules/@fontsource/source-code-pro/files/source-code-pro-latin-600-normal.woff2',
            'node_modules/font-awesome/fonts/fontawesome-webfont.woff2'
          ],
          dest: '_site/assets/css/files'
        }
      ]
    }),
    wrap(),
    nodeResolve({ browser: true }),
    commonjs(),
    process.env.BUILD === 'production' ? babel({ babelHelpers: 'bundled', presets: ['@babel/preset-env'] }) : '',
    process.env.BUILD === 'production' ? terser() : ''
  ]
}
